<?php
/**
 * Created by PhpStorm.
 * User: ngrudenko
 * Date: 24.12.17
 * Time: 17:06
 */
namespace NPParser;

use NPParser\Interfaces\IAbstractParser;
use NPParser\Interfaces\INPParser;
use NPParser\Parsers\CategoriesParser;
use NPParser\Parsers\EventsParser;
use NPParser\Parsers\PlacesParser;

class NPParserFacade implements INPParser
{
	/**
	 * @var IAbstractParser
	 */
	private $__catsPr;

	/**
	 * @var IAbstractParser
	 */
	private $__eventsPr;

	/**
	 * @var IAbstractParser
	 */
	private $__placesPr;

	public function __construct()
	{
		$this->__catsPr = new CategoriesParser();
		$this->__eventsPr = new EventsParser();
		$this->__placesPr = new PlacesParser();
	}

	public function getCategories()
	{
		return $this->__catsPr->get();
	}

	public function getEvents()
	{
		return $this->__eventsPr->get();
	}

	public function getPlaces()
	{
		return $this->__placesPr->get();
	}
}