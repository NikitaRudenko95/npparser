<?php
/**
 * Created by PhpStorm.
 * User: ngrudenko
 * Date: 13.01.18
 * Time: 19:10
 */

namespace NPParser;

use GuzzleHttp\Client;

class UtilsService
{
	private static $__instance = null;

	private $__http;

	// https://developers.google.com/maps/documentation/geocoding/start
	const ADDRESS_TO_COORDS_API_METHOD = 'GET';
	const ADDRESS_TO_COORDS_API_URL = 'https://maps.google.com/maps/api/geocode/json';
	const GOOGLE_API_KEY = 'AIzaSyDysZMh3uBd-HykDU-SGB2tscop_DkofVA';
	const GOOGLE_API_CODE_SUCCESS = 'OK';
	const GOOGLE_API_RESP_LANG = 'ru';

	private function __construct() {
		$this->__http = new Client([
			'base_uri' => self::ADDRESS_TO_COORDS_API_URL
		]);
	}

	protected function __clone() {}

	static public function getInstance()
	{
		if (self::$__instance !== null) {
			return self::$__instance;
		}

		return new self();
	}

	public function getCoordsFromAddress(string $address)
	{
		$response = $this->__http->request(
			self::ADDRESS_TO_COORDS_API_METHOD,
			self::ADDRESS_TO_COORDS_API_URL,
			[
				'query' => [
					'language' => self::GOOGLE_API_RESP_LANG,
					'key' => self::GOOGLE_API_KEY,
					'address' => $address
				]
			]
		);

		$result = json_decode($response->getBody()->getContents());

		if ($result->status !== self::GOOGLE_API_CODE_SUCCESS) {
			sleep(0.5);
			return $this->getCoordsFromAddress($address);
		}

		$data = $result->results[0];

		return [
			'lat' => $data->geometry->location->lat,
			'lon' => $data->geometry->location->lng,
			'country' => $this->getAddressByType('country', $data->address_components),
			'city' => $this->getAddressByType('locality', $data->address_components),
			'street' => $this->getAddressByType('route', $data->address_components),
			'street_number' => $this->getAddressByType('street_number', $data->address_components)
		];
	}

	private function getAddressByType($type, $address)
	{
		foreach ($address as $info) {
			if (in_array($type, $info->types)) {
				return $info->short_name;
			}
		}
	}
}