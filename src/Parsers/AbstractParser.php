<?php
/**
 * Created by PhpStorm.
 * User: ngrudenko
 * Date: 24.12.17
 * Time: 17:47
 */

namespace NPParser\Parsers;


use GuzzleHttp\Client;
use NPParser\UtilsService;
use NPParser\Interfaces\IAbstractParser;

abstract class AbstractParser implements IAbstractParser
{
	const CITIES_FILE_PATH = __DIR__ . '/../../cities.json';

	private static $API_BASE_URL = 'nightparty.ru/';
	private static $API_BASE_METHOD = 'GET';
	protected $currentPage = 0;
	protected $currentCityInfo;
	protected $currentCityIndex = 0;

	protected static $API_PATH = '';
	private static $httpOpts = [
		'headers' => [
			'content-type' => 'application/json'
		]
	];

	/**
	 * @var Client
	 */
	protected $__http;

	/**
	 * @var UtilsService
	 */
	protected $__utils;

	/**
	 * @var string[]
	 */
	protected $__cities;

	public function __construct()
	{
		$this->createHttpInst();
		$this->createUtilsInst();
		$this->getCities();
	}

	protected function getByCity() {
		if (empty($this->currentCityInfo)) {
			$this->currentCityInfo = $this->__cities[0];
		}

		return $this->getEntities();
	}

	protected function getEntities() {
		$response = $this->makeRequest();
		$result = json_decode($response->getBody()->getContents());

		$this->setPage($result->next);

		$hasNextCity = array_key_exists($this->currentCityIndex + 1, $this->__cities);
		$isDataEnded = $this->currentPage < 0;

		if ($isDataEnded && !$hasNextCity) {
			return false;
		} elseif ($isDataEnded && $hasNextCity) {
			$this->currentPage = 0;
			$this->currentCityIndex += 1;
			$this->currentCityInfo = $this->__cities[$this->currentCityIndex];
		}

		return $result;
	}

	/**
	 * @param $opts
	 * @return mixed
	 */
	protected function makeRequest($opts = [])
	{
		$fullUrl = $this->getFullAPIUrl();
		$fullOpts = array_merge(self::$httpOpts, $opts);

		var_dump($fullUrl);
		return $this->__http->request(self::$API_BASE_METHOD, $fullUrl, $fullOpts);
	}

	protected function setPage($nextUrl)
	{
		preg_match('/page\=(\d+)/', $nextUrl, $matches);
		$this->currentPage = !empty($matches) ? $matches[1] : -1;
	}

	private function createHttpInst()
	{
		$this->__http = new Client();
	}

	private function createUtilsInst()
	{
		$this->__utils = UtilsService::getInstance();
	}

	private function getCities() {
		$data = file_get_contents(self::CITIES_FILE_PATH);
		$this->__cities = json_decode($data);
	}

	private function getFullAPIUrl(): string
	{
		$city = $this->currentCityInfo->code;
		$url = 'https://' . $city . '.' . self::$API_BASE_URL;
		$path = static::$API_PATH;
		$page = $this->currentPage;

		if ($page === 0) {
			return $url . $path;
		}

		return $url . $path . '?page=' . $page;
	}
}