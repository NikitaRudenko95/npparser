<?php
/**
 * Created by PhpStorm.
 * User: ngrudenko
 * Date: 24.12.17
 * Time: 17:46
 */

namespace NPParser\Parsers;

class PlacesParser extends AbstractParser
{
	protected static $API_PATH = 'topic/places';

	public function get()
	{
		$result = $this->getByCity();
		$hasPlaces = !empty($result->results);

		if ($hasPlaces) {
			return $this->addLocation($result);
		}

		return $result;
	}

	private function addLocation($result)
	{
		$places = $result->results;
		foreach ($places as $place) {
			if (empty($place->address)) {
				continue;
			}

			$place->address = $this->__utils->getCoordsFromAddress($place->address);
		}

		return $places;
	}
}