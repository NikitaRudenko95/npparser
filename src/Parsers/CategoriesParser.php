<?php
/**
 * Created by PhpStorm.
 * User: ngrudenko
 * Date: 25.12.17
 * Time: 0:24
 */

namespace NPParser\Parsers;


class CategoriesParser extends AbstractParser
{
	protected static $API_PATH = 'topic/categories/';

	public function get(array $config = [])
	{
		return $this->getCategories($this->makeRequest());
	}

	public function getByIds(array $ids)
	{
		// TODO: Implement getByIds() method.
	}

	private function getCategories($response)
	{
		$result = json_decode($response->getBody()->getContents());
		return $result->result[0]->tags;
	}
}