<?php
/**
 * Created by PhpStorm.
 * User: ngrudenko
 * Date: 24.12.17
 * Time: 17:46
 */

namespace NPParser\Parsers;


class EventsParser extends AbstractParser
{
	protected static $API_PATH = 'topic/events';

	public function get()
	{
		return $this->getByCity();
	}
}