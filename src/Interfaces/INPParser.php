<?php
/**
 * Created by PhpStorm.
 * User: ngrudenko
 * Date: 24.12.17
 * Time: 17:12
 */

namespace NPParser\Interfaces;


interface INPParser
{
	public function getPlaces();

	public function getEvents();

	public function getCategories();
}