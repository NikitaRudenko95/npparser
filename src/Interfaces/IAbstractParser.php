<?php
/**
 * Created by PhpStorm.
 * User: ngrudenko
 * Date: 24.12.17
 * Time: 17:47
 */

namespace NPParser\Interfaces;

interface IAbstractParser
{
	public function get();
}